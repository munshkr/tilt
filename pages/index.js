import lzwCompress from "lzwcompress";
import getConfig from "next/config";
import dynamic from "next/dynamic";
import Head from "next/head";
import { withRouter } from "next/router";
import PropTypes from "prop-types";
import React from "react";
import Button from "../components/Button";
import ErrorMessage from "../components/ErrorMessage";
import Oscilloscope from "../components/Oscilloscope";
import SynthController from "../components/SynthController";

const { publicRuntimeConfig } = getConfig();
const { assetPrefix } = publicRuntimeConfig;

const URL_VERSION = 1;

const Editor = dynamic(() => import("../components/Editor"), {
  ssr: false,
  loading: () => <span>Loading...</span>
});

const DEFAULT_CONTENT = `// Define variable o to set audio output, like this:
o = ( ((t<<1)^((t<<1)+(t>>7)&t>>12))|t>>(4-(1^7&(t>>19)))|t>>7 ) %64/64
`;

const PlayButton = props => <Button src="play.svg" {...props} />;
const StopButton = props => <Button src="stop.svg" {...props} />;
const ShareButton = props => <Button src="share.svg" {...props} />;

const prelude = `
// constants
const pi = Math.PI;
const twoPi = 2 * pi;

// basic functions
const {
  abs, acosh, acos, asinh, asin, atan2, atanh, cbrt, ceil, cosh, cos, exp,
  floor, log2, log, max, min, pow, round, sign, sinh, sin, sqrt, tanh, tan,
  trunc,
} = Math;

// waveforms
const sine = arg => (Math.sin(arg) + 1) / 2;
const saw = arg => (arg % twoPi) / twoPi;
const pulse = (arg, width) => saw(arg) > (width || 0.5);
const square = arg => pulse(arg, 0.5);
const tri = arg => {
  const r = saw(arg);
  return ((r > 0.5 ? r : 1 - r) - 0.5) * 2;
};
const noise = () => Math.random();

// sequences
const seq = (subdiv, length) => Math.floor((t / (K / subdiv)) % length);
const seq1 = (subdiv, length) => seq(subdiv, length) + 1;
const aseq = (subdiv, array) => array[seq(subdiv, array.length)];
const rseq = (subdiv, array) => array[randInt(subdiv, array.length)];

// envelopes
const env = (subdiv, curve, smooth) => {
  if (typeof curve === "undefined") curve = 1;
  if (typeof smooth === "undefined") smooth = 0.05;
  const tp = (t % (K / subdiv)) / (K / subdiv);
  const mult = tp <= smooth ? tp / smooth : 1;
  return Math.pow(1 - tp, curve) * mult;
};
const invEnv = (subdiv, curve, smooth) => {
  if (typeof curve === "undefined") curve = 1;
  if (typeof smooth === "undefined") smooth = 0.05;
  const tp = (t % (K / subdiv)) / (K / subdiv);
  const mult = tp >= 1 - smooth ? (1 - tp) / smooth : 1;
  return Math.pow(tp, curve) * mult;
};

// randomness
const _prime1 = 1120911527;
const _prime2 = 341225299;
const _prime3 = 3073422643;
const random = (n, seed) =>
  (((n + seed) * (n + seed) * _prime1 + (n + seed) * _prime2) % _prime3) /
  _prime3;
const rand = (subdiv, seed) => {
  if (typeof subdiv === "undefined") subdiv = 1;
  if (typeof seed === "undefined") seed = 0;
  const v = Math.floor(t / (K / subdiv));
  return random(v, seed);
};
const randInt = (subdiv, max, seed) => {
  max = Math.floor(max);
  return Math.floor(rand(subdiv, seed) * max);
};

// freq and midinote conversions
const f = freq => t*(freq/440);
const m = midinote => 2**((midinote-69)/12)*t;
`;

const generateURL = content => {
  const buf = lzwCompress.pack(content);
  const code = btoa(buf);
  return `${assetPrefix}/#v=${URL_VERSION}&c=${code}`;
};

const getHashStringParams = () => {
  const query = window.location.hash;
  return query
    ? (/^[?#]/.test(query) ? query.slice(1) : query).split("&").reduce((params, param) => {
        const newParams = Object.assign({}, params);
        const [key, value] = param.split("=");
        newParams[key] = value ? decodeURIComponent(value.replace(/\+/g, " ")) : "";
        return newParams;
      }, {})
    : {};
};

const getLastContent = () => localStorage.getItem("lastContent");

const setLastContent = content => {
  localStorage.setItem("lastContent", content);
};

class Index extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      audioContext: null,
      content: DEFAULT_CONTENT,
      generator: null,
      isPlaying: false,
      isFlashing: false,
      error: null,
      oscSupported: false
    };
  }

  componentDidMount() {
    let content;

    const query = getHashStringParams();
    // console.log(`query params = ${JSON.stringify(query)}`);

    // If URL contains a "c" param, decode source code
    if (query.c) {
      content = this._decodeCode(query.c);
      // console.log('load code from query params');
    } else {
      // Otherwise, try to get last content from localStorage
      content = getLastContent();
    }

    // If any, set content
    if (content) {
      // console.log('found! set content');
      this.setState({ content });
    }

    this.setState({ oscSupported: this._isOscSupported() });

    window.addEventListener("deviceorientation", this._onOrientation);
  }

  _onChange = text => {
    setLastContent(text);
    this.setState({ content: text, error: null });
  };

  _onEval = editor => {
    const content = editor.getValue();
    this.eval(content);
  };

  _onStop = () => {
    this.stop();
  };

  _onPlayButtonClick = () => {
    const { content } = this.state;
    this.eval(content);
  };

  _onStopButtonClick = () => {
    this.stop();
  };

  _onShareButtonClick = () => {
    const { router } = this.props;
    const { content } = this.state;
    const url = generateURL(content);
    router.replace(url, url, { shallow: true });
  };

  _onOrientation = event => {
    const x = event.beta; // In degree in the range [-180,180]
    const y = event.gamma; // In degree in the range [-90,90]

    // Because we don't want to have the device upside down
    // We constrain the x value to the range [-90,90]
    // if (x > 90) {
    //   x = 90;
    // }
    // if (x < -90) {
    //   x = -90;
    // }

    window.gX = (x + 180) / 360;
    window.gY = (y + 90) / 180;
  };

  // eslint-disable-next-line class-methods-use-this
  _isOscSupported() {
    // Looks like iOS does not support something related to canvas...?
    // FIXME: Understand which feature is neeeded for drawing canvas and check
    // for that, instead of sniffing user agent...
    const iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    return !iOS;
  }

  _flash() {
    this.setState({ isFlashing: true });
    setTimeout(() => this.setState({ isFlashing: false }), 500);
  }

  play() {
    let { audioContext } = this.state;
    if (!audioContext) {
      audioContext = new (window.AudioContext || window.webkitAudioContext)();
    }
    this.setState({ audioContext, isPlaying: true });
  }

  stop() {
    const { isPlaying } = this.state;
    if (isPlaying) {
      this._flash();
      this.setState({ isPlaying: false });
    }
  }

  eval(content) {
    if (this._tryEval(content)) {
      const generator = null;
      // eslint-disable-next-line no-eval
      eval(`generator = (t, r, K) => {
        let o = 0;
        ${prelude};
        ${content};
        return [o, r, K];
      }`);
      this.setState({ generator });
      this._flash();
      this.play();
    }
  }

  _tryEval(content) {
    try {
      // parameters
      // eslint-disable-next-line no-unused-vars
      const t = 0;
      // eslint-disable-next-line no-unused-vars
      const r = 1;
      // eslint-disable-next-line no-unused-vars
      const K = 0;
      // global variables and functions
      // eslint-disable-next-line no-unused-vars
      const o = 0;
      // content
      // eslint-disable-next-line no-eval
      eval(
        `${prelude};
        ${content}`
      );
      return true;
    } catch (err) {
      this.setState({ error: err.message });
      // console.error(err);
      return false;
    }
  }

  _decodeCode(code) {
    try {
      const buf = atob(code)
        .split(",")
        .map(parseFloat);
      return lzwCompress.unpack(buf);
    } catch (err) {
      this.setState({ error: `(Invalid URL) ${err.message}` });
      return null;
    }
  }

  render() {
    const {
      audioContext,
      isPlaying,
      generator,
      isFlashing,
      content,
      error,
      oscSupported
    } = this.state;

    return (
      <div className={isFlashing ? "flash" : ""}>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta charSet="utf-8" />
          <title>Tilt</title>
        </Head>

        <Editor
          ref={c => {
            this.editor = c;
          }}
          onEval={this._onEval}
          onStop={this._onStop}
          onChange={this._onChange}
          content={content}
        />
        <SynthController
          ref={c => {
            this.synth = c;
          }}
          audioContext={audioContext}
          isPlaying={isPlaying}
          generator={generator}
        />
        <div className="controls">
          <PlayButton onClick={this._onPlayButtonClick} />
          <StopButton onClick={this._onStopButtonClick} disabled={!isPlaying} />
          <ShareButton onClick={this._onShareButtonClick} />
        </div>

        {oscSupported && audioContext ? (
          <Oscilloscope audioContext={audioContext} synth={this.synth} isPlaying={isPlaying} />
        ) : (
          ""
        )}
        {error ? <ErrorMessage message={error} /> : ""}

        <style global jsx>
          {`
            body {
              background-color: transparent;
              margin: 0;
            }
          `}
        </style>
        <style jsx>
          {`
            .controls {
              position: absolute;
              right: 1.5em;
              bottom: 1em;
              z-index: 2;
            }

            .flash {
              -webkit-animation-name: flash-animation;
              -webkit-animation-duration: 0.2s;
              animation-name: flash-animation;
              animation-duration: 0.2s;
            }

            @-webkit-keyframes flash-animation {
              from {
                background: black;
              }
              to {
                background: default;
              }
            }

            @keyframes flash-animation {
              from {
                background: black;
              }
              to {
                background: default;
              }
            }
          `}
        </style>
      </div>
    );
  }
}

Index.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  router: PropTypes.object.isRequired
};

export default withRouter(Index);
